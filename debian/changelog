ironseed (0.3.6-6) UNRELEASED; urgency=medium

  * hurd-i386 architecture should also be possible, if/when it has working
    dependencies
  * test should not be flaky (on supported architectures)

 -- Matija Nalis <mnalis-deb@voyager.hr>  Fri, 05 Feb 2021 14:36:37 +0100

ironseed (0.3.6-5) unstable; urgency=medium

  * mipsel architecture does not yet have working gdc (see Bug#980204),
    so skip rebuild of game datafiles which require that for now.

 -- Matija Nalis <mnalis-deb@voyager.hr>  Sat, 16 Jan 2021 04:00:00 +0100

ironseed (0.3.6-4) unstable; urgency=medium

  * enable extra little-endian architectures theoretically supported by
    freepascal (mipsel mips64el kfreebsd-amd64 kfreebsd-i386 riscv64)

 -- Matija Nalis <mnalis-deb@voyager.hr>  Thu, 14 Jan 2021 03:44:28 +0100

ironseed (0.3.6-3) unstable; urgency=medium

  * enable ppc64el architecture too

 -- Matija Nalis <mnalis-deb@voyager.hr>  Sat, 09 Jan 2021 01:37:01 +0100

ironseed (0.3.6-2) unstable; urgency=medium

  * update debian/rules for faster startup, due to lintian warning
    debian-rules-sets-dpkg-architecture-variable
  * build AND test only on known-to-work architectures
  * change DEB_HOST_ARCH Makefile variable name, as it has reserved &
    unintended side effects (which made armhf/armel packages uninstallable)

 -- Matija Nalis <mnalis-deb@voyager.hr>  Tue, 05 Jan 2021 09:55:58 +0100

ironseed (0.3.6-1) unstable; urgency=medium

  * ppc64el architecture does not yet have working gdc (see Bug#978153),
    so skip rebuild of game datafiles which require it, for now.
  * try for "Architecture: any" again, as droping ppc64el didn't help with
    migration to testing, and upstream 0.3.6 should be able to build on more
    architectures (like mipsel)
  * extra build hardening on amd64 (-pie)
    https://wiki.debian.org/Hardening
  * declare test as flaky for now, until we figure out ppc64el failures
  * New upstream release

 -- Matija Nalis <mnalis-deb@voyager.hr>  Tue, 05 Jan 2021 08:07:49 +0100

ironseed (0.3.5-2) unstable; urgency=medium

  * limit architectures on which we successfully build, to avoid being
    blocked for migration to testing (due to ppc64el erroneously being
    marked as autopkgtest failure, while package was never even built
    successfully on that architecture)
  * providing "Multi-Arch: foreign" even for "Architecture: all"
    ironseed-data, as suggested by:
    https://tracker.debian.org/pkg/ironseed
    https://wiki.debian.org/MultiArch/Hints

 -- Matija Nalis <mnalis-deb@voyager.hr>  Thu, 31 Dec 2020 00:22:10 +0100

ironseed (0.3.5-1) unstable; urgency=medium

  * New upstream release
  * do not fail when removing duplicate manpage if it is already gone

 -- Matija Nalis <mnalis-deb@voyager.hr>  Sun, 27 Dec 2020 03:34:52 +0100

ironseed (0.3.4-1) unstable; urgency=medium

  * New upstream release
  * implement basic working autopkgtest (start game, check savefiles)

 -- Matija Nalis <mnalis-deb@voyager.hr>  Tue, 15 Dec 2020 04:32:55 +0100

ironseed (0.3.2-2) unstable; urgency=low

  * Initial release. Closes: #971924

 -- Matija Nalis <mnalis-deb@voyager.hr>  Sat, 12 Dec 2020 15:26:26 +0100
